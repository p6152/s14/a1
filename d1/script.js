/*console.log("Hello World! This is from external JS");*/

// Writing a comment
	// single line ctrl + /
	// multi-line comment ctrl + shift + /
/*
This is multi-line 
comments
*/

/*

	Syntax and Statements

	Syntax 
	- set of rules of how of codes should be written correctly

	Statement
	- set of instructions, ends with semicolon
*/

// alert("Good afternoon");

/*
	Variables
		- a container that holds value
*/

	/*let myName = "Prince";
	console.log(myName);*/

/* Anatomy 

	**Declaration:

	let <variable name>

		- declaration
		- declaring a variable
		- cannot re-decclare same variable name under same scope
Exmaple
	let car;
	console.log(car);

	let car = "mercedez";
	console.log(car);


	**Initialization:

		- initializing a variable with a value

Example:
	let phone = "IPhone";
	console.log(phone);

	**Re-assignment
		-assigning new value to a variable using equal operator

	myName = "IPhone";
	console.log(myName);
*/

/*
// Why do you think variable is important
	// reusable

	let brand = "Mac";
	brand = "acer";
	console.log(brand);
	console.log(brand);
	console.log(brand);
	console.log(brand);
	console.log(brand);
	console.log(brand);
	console.log(brand);
	console.log(brand);
	console.log(brand);
	console.log(brand);
*/

	/*console.log(lastName);
		// without let keyword, return will be "NOT DEFINED"


	let firstName;
	console.log(firstName);
		// returns "UNDEFINED"*/

/*
	Naming Convention

	1. Case Sensitive

		Example:
		let color = "pink";
		let Color = "blue";

		console.log(color);
		console.log(Color);

	2. Variable name must be relevant to its actual value

		Example:

		let value = "Robin";
		let goodThief = "Robin";
		let bird = "Robin";

	3. Camel Case Notation

		let capitalcityofthephilippines = "Manila City";

		//This is readable
		let capitalCityOfThePhilippines = "Manila City";

	4. Should not begin with number and @ sign

		Example:
		// acceptable
		let year3 = 3;
		console.log(year3);	

		// acceptable
		let _year = 3;
		console.log(_year);

		// acceptable
		let &year = 3;
		console.log(&year);

		// not acceptable
		let 3year = 3;
		console.log(year3);

		// not acceptable
		let @year = 3;
		console.log(@year);

		// single and double quotes are accepted

		let flower = "rose";
		console.log(flower);

		let flower2 = 'sunflower';
		console.log(flower2);

*/
	/*console.log('She said, "Hello There"');
	console.log('Hello, I\'m Prince');

	// ES6 updates
		// Template literals - using backticks ``

		console.log(`"She said, "Hello There"`);
		console.log(`Hello, I'm Prince`);
*/

/*
	Constant 
		- a type of variable that holds value but it cannot be changed nor re-assigned

		// Re-assignment is not allowed
		PI = 14;
		console.log(PI);

		// receive an error, it should have a value
		const boilingPoint;
		console.log(boilingPoint);


		// Correct syntax
		const boilingPoint = 100;
		console.log(boilingPoint);
*/


/*

MINI-ACTIVITY 

	Declare variables called
		country
		continent
		population

		Assign their values according to your own contry (population as of 2020)

		Log their values to the console

		let country = `Philippines`;
		let continent = `Asia`;
		let population = `109.6 Million`;

		console.log(country);
		console.log(continent);
		console.log(population);
*/
/*
	Data Types

	1. String
		- sequence of characters
		- they are always wrapped with quotes or backticks

		Example:

		let food =`Sinigang`;
		let Name = `Prince`;

	2. Number
		-
		
		Example:
		let x = 4;
		let y = 8;

		let result = x + y;
		console.log(result);

		let a = `2`;
		let b = `8`;
		let c = `3`;

		let result1 = a + b + c; //concatenation
		console.log(result1);

		let result2 = a - b - c; //minus
		console.log(result2);

	3. Boolean
		- values TRUE or FALSE

		let isEarly = "true";
		console.log(isEarly);

		let areLate = "false";
		console.log(areLate);

	4. Undefined
		- variable has been declared however no value yet
		- empty value

		let job;
		console.log(job);

	5. Null
		- empty value
		- it is used to represent an empty value

	6. BigInt()
		- large integers more than the data type can hold

	7. Object
		- one variable can handle/contain several different types of data
		- it is wrapped with curly braces
		- has property and value

		Example:
		let user = {
			firstName: `Prince`,
			lastName: `Monisit`,
			email: [`princeandwill@gmail.com`, `pmonisit.a6162831@gmail.com`],
			age: 29,
			isStudent: true,
			spouse: null
		};
*/

/*
	Special type of object

	- Array
		// Collection of related data
		// Enclosed with square brackets
		let fruits = [`Apple`, `Banana`, `Strawberry`];
		let grades = [89, 90, 92, 100, 75];
*/

/*	typeof Operators
		- evaluates what type of data and returns a string

		let animal = `dog`;
		let age = 16;
		let isHappy = true;
		let him;
		let spouse = null;
		let admin = {
			name: `admin`,
			isAdmin: true,
		}
		let ave = [83.5, 89.6, 94.2];

		console.log(typeof animal);
		console.log(typeof age);
		console.log(typeof isHappy);
		console.log(typeof him);
		console.log(typeof spouse);
		console.log(typeof admin);
		console.log(typeof ave);
*/

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
	JavaScript Functions
			- reusable, convenient because it helps us save time than repeating same task.

			function sayHello() {
				console.log(`Hello World`);
			}
			sayHello();
			sayHello();

	Anatomy of a function

			Syntax of a function

					function <fname> () {
							statement / code block
					}
			
			1. Declaration 
					- function keyword
					- function name + parenthesis
							- parameters inside the parenthesis
					- curly braces
							- determines its codeblock
							- statements are written inside the codeblock

			2. Invocation
					- invoke / call the function
					- by invoking the function it execute the code block.

					- function name + parenthesis
							arguments inside the parenthesis

*/	
						//parameter
		/*function greeting(name) {
			console.log(`Hi ${name}!`);
		}
				//argument
		greeting(`Prince`);
		greeting(`Andwill`);

		//Example

		function getProduct(x,y) {
			console.log(`The product of ${x} and ${y} is ${x*y}`)
		}

		getProduct(4,4);
		getProduct(7,9);*/
	


	function getTotal (x,y,z) {
		console.log(`The total of ${x} + ${y} + ${z} is ${x+y+z}`);
		// return x + y + z
		// return `${x + y + z}`
	}

	// getTotal(6,8,9);

	/*
		Function with return keyword

		function sayName(fname, lname) {
			return `Hi, my name is ${fname} ${lname}`
		}

		// First method
		console.log(sayName(`prince`, `andwill`));

		// Second method - storing invocation 

		let result = sayName(`prince`, `monisit`);
		console.log(result);

	*/

	function getInfo(name, age) {

		console.log(
				`Hi' I'm ${name} \n My age + 10 is ${age+10}`
			)
		return (
			`Hi' I'm ${name} My age + 10 is ${age+10}`
		)
	}
	getInfo(`Prince`, 17);
		
		
	






	


