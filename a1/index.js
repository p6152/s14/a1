	 let firstName = `John`;
	 let lastName = `Smith`;
	 let age = 30;
	 let hobbies = [`Biking`, ` Mountain Climbing`, ` Swimming`];
	 let workAddress = {
	 	houseNumber: 32,
	 	street: `Washington`,
	 	city: `Lincoln`,
	 	state: `Nebraska`
	 };
	 let isMarried = true;


	 // Function for printUserInfo 
	 function printUserInfo () {
	 	console.log(
	 	    `First Name: ${firstName}
	 	    \nLast Name: ${lastName}
	 	    \nAge: ${age}
	 	    \nHobbies: ${hobbies}
	 	    \nWork Address: ${workAddress}`
	 	 )
	 }
	 printUserInfo();

	 
	 // Function for returnFunction 
	 function returnFunction (){

		 return `${firstName} ${lastName} is ${age} years of age\nHobbies\n${hobbies}\nWork Address\n${workAddress}\nThe value of Married is: ${isMarried}`

	}
	console.log(returnFunction());


	 


	

